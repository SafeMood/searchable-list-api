# Searchable list API :zap:

A simple API that returns a list of products their categories using Laravel 8.

## Clone project

```bash
git clone git@gitlab.com:SafeMood/searchable-list-api.git
```

## Installation

Use the package manager [composer](https://getcomposer.org/) to install dependencies.

```bash
cd searchable-list-api
```

```bash
composer install
```

### make sure you to ceate a database with same name as the .env file

```bash
DB_DATABASE=api
```

## Request API products endpoint from a browser

```bash
 http://127.0.0.1:8000/api/products
```

## Response

Requests will receive an HTTP 200 Status Code with the following response

```json
{
    "data": {
        "products": [
            {
                "id": 1,
                "name": "commodi",
                "price": 35.73,
                "description": "Consequatur non asperiores quisquam atque ipsam accusantium. Est repellendus eius voluptates sapiente in. Cum quam voluptatum recusandae ullam. Et provident dolorum delectus commodi.",
                "available": 1,
                "category": {
                    "id": 1,
                    "name": "Home"
                }
                // paginated by 20
            }
        ],
        "categories": [
            {
                "id": 1,
                "name": "Home"
            },
            {
                "id": 2,
                "name": "Pet supplies"
            },
            {
                "id": 3,
                "name": "Sports"
            },
            {
                "id": 4,
                "name": "Fashion"
            },
            {
                "id": 5,
                "name": "Office"
            }
        ]
    },
    "links": {
        "first": "http://127.0.0.1:8000/api/products?page=1",
        "last": null,
        "prev": null,
        "next": "http://127.0.0.1:8000/api/products?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "path": "http://127.0.0.1:8000/api/products",
        "per_page": 20,
        "to": 20,
        "of": 100
    }
}
```

## Testing

make sure you are in the main project folder

```bash
  php artisan test
```

## Debugging using telescope

access this link

```bash
http://127.0.0.1:8000/telescope
```

### keep an eye on any N+1 or performance problem

```bash
http://127.0.0.1:8000/telescope/queries
```

## License

[MIT license](https://opensource.org/licenses/MIT).
