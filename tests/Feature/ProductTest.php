<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * test if user can list products.
     *
     * @return void
     */
    public function test_user_can_list_products()
    {

        $response = $this->json('GET', '/api/products');
        $response->assertOk();
    }


    /**
     * test json data structure.
     *
     * @return void
     */
    public function test_json_data_structure()
    {

        $response = $this->get('/api/products');


        $expectedJsonResponseStructure = [
            "data" => [
                "products" => [
                    "*" => [
                        "id",
                        "name",
                        "price",
                        "description",
                        "available",
                        "category" => [
                            "id",
                            "name"
                        ]
                    ]
                ],
                "categories" => [
                    "*" => [
                        "id",
                        "name"
                    ]
                ]

            ],
            "links" => [
                "first",
                "last",
                "prev",
                "next"
            ],
            "meta" => [
                "current_page",
                "from",
                "path",
                "per_page",
                "to",
                "of",

            ]
        ];
        $response->assertJsonStructure($expectedJsonResponseStructure);
    }
}
